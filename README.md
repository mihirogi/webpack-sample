# 概要

別で練習してるElectronアプリにおいて、型とかなくてつらくなってきたので、TypeScriptなるものを使う  
少し調べたら、TypeScript + webpackというのがたくさん出てきた。
なので、TypeScript + webpackを使った開発の練習をする  

ところどころ語尾が違うのは、サイトの内容をそのままコピペしてきたりするから

## webpackの練習
ここを参照しながら  
https://ics.media/entry/12140/2

### 準備

* TypeScriptの導入
* 練習用ソースコードの準備
* webpackの導入 

#### TypeScript

```
npm init
npm install --save-dev typescript
```

* `--save-dev` : package.jsonの依存関係に追記される


#### ソースコード

ソースコード用のファイル
```
mkdir ./src
touch ./src/index.js
touch ./src/sub.js
```

見慣れない`export`がついてるけど、ES Modulesの仕様に沿った記法らしい

```
(index.js)

// import 文を使って sub.js ファイルを読み込む。
import {hello} from './sub';
 
// sub.jsに定義されたJavaScriptを実行する。
hello();
```

```
(sub.js)

// export文を使ってhello関数を定義する。
export function hello() {
  alert('helloメソッドが実行された。');
}
```

JavaScriptモジュールはこのままだと古いブラウザ（例：IE 11）で使用できない。  
そのため、ブラウザが解釈できる形に変換する必要があります。  
そこで登場するのがwebpackです。  

#### webpack

webpackを使うと、JavaScriptモジュールをブラウザで扱える形に変換できます。  
index.jsのようにメインとなる処理を行うJavaScriptファイル「エントリーポイント」と呼びます。  
エントリーポイントをコマンドでビルドします。

導入  
`npm install --save-dev webpack webpack-cli`

### 試してみる

ビルドしてみる  
`npx webpack`

出力
```
Hash: fe8af88f58be55517b41
Version: webpack 4.16.3
Time: 864ms
Built at: 2018-08-02 11:06:25
  Asset       Size  Chunks             Chunk Names
main.js  977 bytes       0  [emitted]  main
Entrypoint main = main.js
[0] ./src/index.js + 1 modules 193 bytes {0} [built]
    | ./src/index.js 105 bytes [built]
    | ./src/sub.js 88 bytes [built]
```

`./dist`っていうフォルダが出てきてて、中に、`main.js`ってファイルができてる  
改行やインデントのないコンパクトなものができてた  
```
!function(e){var t={};function r(n){if(t[n])return t[n].exports;var o=t[n]={i:n,l:!1,exports:{}};return e[n].call(o.exports,o,o.exports,r),o.l=!0,o.exports}r.m=e,r.c=t,r.d=function(e,t,n){r.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:n})},r.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},r.t=function(e,t){if(1&t&&(e=r(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var n=Object.create(null);if(r.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var o in e)r.d(n,o,function(t){return e[t]}.bind(null,o));return n},r.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return r.d(t,"a",t),t},r.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},r.p="",r(r.s=0)}([function(e,t,r){"use strict";r.r(t),alert("helloメソッドが実行された。")}]);
```

実行確認用html  
アラート出たから動いてる
```
(index.html)

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script defer src="./dist/main.js"></script>
</head>
<body>

</body>
</html>
```

npx webpackコマンドでビルドするのもシンプルですが、実際の開発ではnpm scriptsを使う方が便利です。  
npm scriptsとはコマンドのショートカット（エイリアス）を貼るための機能。  
package.jsonファイルのscriptsには、webpackのビルドコマンドを追加します。

```
(package.json)

{
  "name": "typescript-sample",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "webpack"
  },
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "webpack": "^4.16.3",
    "webpack-cli": "^3.1.0"
  }
}

```

実行してみる  
`npm run build`

出力  
```
npm WARN npm npm does not support Node.js v7.8.0                                                                                                                                                                                      
npm WARN npm You should probably upgrade to a newer version of node as we                                                                                                                                                             
npm WARN npm can't make any promises that npm will work with this version.                                                                                                                                                            
npm WARN npm Supported releases of Node.js are the latest release of 6, 8, 9, 10, 11.                                                                                                                                                 
npm WARN npm You can find the latest version at https://nodejs.org/                                                                                                                                                                   
                                                                                                                                                                                                                                      
> typescript-sample@1.0.0 build C:\Users\k.soga\work\study\electron\typescript-sample                                                                                                                                                 
> webpack                                                                                                                                                                                                                             
                                                                                                                                                                                                                                      
Hash: fe8af88f58be55517b41                                                                                                                                                                                                            
Version: webpack 4.16.3                                                                                                                                                                                                               
Time: 230ms                                                                                                                                                                                                                           
Built at: 2018-08-02 11:14:36                                                                                                                                                                                                         
  Asset       Size  Chunks             Chunk Names                                                                                                                                                                                    
main.js  977 bytes       0  [emitted]  main                                                                                                                                                                                           
Entrypoint main = main.js                                                                                                                                                                                                             
[0] ./src/index.js + 1 modules 193 bytes {0} [built]                                                                                                                                                                                  
    | ./src/index.js 105 bytes [built]                                                                                                                                                                                                
    | ./src/sub.js 88 bytes [built]                                                                                                                                                                                                   
                                                                                                                                                                                                                                      
WARNING in configuration                                                                                                                                                                                                              
The 'mode' option has not been set, webpack will fallback to 'production' for this value. Set 'mode' option to 'development' or 'production' to enable defaults for each environment.                                                 
You can also set it to 'none' to disable any default behavior. Learn more: https://webpack.js.org/concepts/mode/                                                                                                                      
```

何かいろいろ言われてる  
modeには、`development`と`product`あるので設定しろってことらしい。



### webpackの設定ファイルを作ってみる

`webpack.config.js`を作る  
`touch webpack.config.js`

```
(webpack.config.js)

module.exports = {
 
  // メインとなるJavaScriptファイル（エントリーポイント）
  entry: `./src/index.js`, <- これが書いてないとデフォルトで「src/index.js」が指定されるらしい
 
  // ファイルの出力設定
  output: {
    //  出力ファイルのディレクトリ名
    path: `${__dirname}/dist`, <- これがないと、デフォルトで「dist/main.js」となるらしい
    // 出力ファイル名
    filename: 'test-output.js' <- test-outputにしてみた
  },
};

```

実行してみる  
`npm run build`

出力
```
Hash: 97b0050f1927de7ffee8
Version: webpack 4.16.3
Time: 778ms
Built at: 2018-08-02 11:26:56
         Asset       Size  Chunks             Chunk Names
test-output.js  977 bytes       0  [emitted]  main
Entrypoint main = test-output.js
[0] ./src/index.js + 1 modules 193 bytes {0} [built]
    | ./src/index.js 105 bytes [built]
    | ./src/sub.js 88 bytes [built]
```
おお、できてる
```
$ ls dist/
main.js  test-output.js
```

### webpackのもうちょっと便利な使い方

これに関係する。
>何かいろいろ言われてる  
>modeには、`development`と`product`あるので設定しろってことらしい。

 
webpackの設定ファイルには次のように記述します。  
modeにdevelopmentを記述することでソースマップを有効にします。  
逆に、modeの部分でproductionを指定することで、JavaSciptのコードを圧縮できます。  
開発時にはdevelopmentを指定し、ウェブサイト公開時にはproductionに設定するのがいいでしょう。

```
(webpack.config.js)

module.exports = {
module.exports = {

    // メインとなるJavaScriptファイル（エントリーポイント）
    entry: `./src/index.js`,

    // ファイルの出力設定
    output: {
        //  出力ファイルのディレクトリ名
        path: `${__dirname}/dist`,
        // 出力ファイル名
        filename: 'test-output.js'
    },
    // モード値を production に設定すると最適化された状態で、
    // development に設定するとソースマップ有効でJSファイルが出力される
    mode: 'development' <- これがふえた
};

```

`development` を指定したときのファイルサイズ  
```
Hash: 4384a70211b99e9b91a1
Version: webpack 4.16.3
Time: 176ms
Built at: 2018-08-02 11:40:06
         Asset      Size  Chunks             Chunk Names
test-output.js  4.61 KiB    main  [emitted]  main
Entrypoint main = test-output.js
[./src/index.js] 105 bytes {main} [built]
[./src/sub.js] 88 bytes {main} [built]
```

`production` を指定したときのファイルサイズ  
```
Hash: 8249a18aeba8b626e522
Version: webpack 4.16.3
Time: 235ms
Built at: 2018-08-02 11:40:57
         Asset       Size  Chunks             Chunk Names
test-output.js  977 bytes       0  [emitted]  main
Entrypoint main = test-output.js
[0] ./src/index.js + 1 modules 193 bytes {0} [built]
    | ./src/index.js 105 bytes [built]
    | ./src/sub.js 88 bytes [built]
```

たしかにちっちゃくなった！！！！！


### もうちょっと便利な開発環境構築

毎回`npm run build`はちょっとめんどくさい。  
(私は特に、スタイル変更とかちまちま見ながら変えたりする)  
「webpack-dev-server」はとても便利な機能です。わずかな設定でできるので構築してみましょう。

導入  
`npm install --save-dev webpack-dev-server`

開発用ローカルサーバが立ち上がるような設定を追記する
```
(package.json)

{
  "name": "typescript-sample",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "webpack",
    "start": "webpack-dev-server" <- これを追記
  },
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "webpack": "^4.16.3",
    "webpack-cli": "^3.1.0",
    "webpack-dev-server": "^3.1.5"
  }
}
```

自動的にブラウザが立ち上がるようにする  
```
(webpack.config.js)

module.exports = {

    // メインとなるJavaScriptファイル（エントリーポイント）
    entry: `./src/index.js`,

    // ファイルの出力設定
    output: {
        //  出力ファイルのディレクトリ名
        path: `${__dirname}/dist`,
        // 出力ファイル名
        filename: 'main.js'
    },
    mode: 'production',
    devServer: { <- これを追記
        contentBase: 'dist',
        open: true
    }

};

```
実行すると、edgeが立ち上がってきた・・・。

ソースコードを変更すると、ブラウザがリロードもされるので、確認用にソース修正  
```
(sub.js)

// export文を使ってhello関数を定義する。
export function hello(message) {
    document.body.innerHTML = message;
    console.log(message);
}

```

`index.html`を`/dist`の下に移動する  

### おまけ

ブラウザで確認する必要のない場合は、以下でjsだけビルドできるらしい  
`npx webpack --watch`

package.jsonにタスク？を追加してあげればもっと使いやすく！

```
(package.json)


  "scripts": {
    "build": "webpack",
    "watch": "webpack --watch"
  },
  "devDependencies": {
    "webpack": "^4.5.0",
    "webpack-cli": "^2.0.14"
  }
}

```

## よくわからなかった

* ソースマップって何だろう